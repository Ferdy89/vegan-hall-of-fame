---
title: "Deli slices"
summary: "Turkey style seitan"
date: 2022-12-08T20:26:01-08:00
featured_image: images/sliced.jpg
time: "90m"
---

This seitan has the texture of roast beef but the flavor of smoked turkey. It's
very savory but not salty. It's an adaptation of [the recipe from "It doesn't
taste like
chicken"](https://itdoesnttastelikechicken.com/homemade-vegan-deli-slices-smoked-seitan-turkey/#recipe)
with some tweaks I've iterated myself over some months. I hope the ingredients
aren't too difficult to find.

It's a bit messy to make but it doesn't take too long (most of the time is
spent boiling or cooling, the actual cooking might be 20 minutes). The result
is about 1 kg of seitan that works fantastic sliced in sandwiches but also cut
into cubes and fried.

## Ingredients

* 1 block of tofu with the water it comes in (about 580 gr)
* 36 gr of miso paste (about 2 tbsp)
* 18 gr of onion powder (about 2 tbsp)
* 14 gr of garlic powder (about 1.5 tbsp)
* 23 gr of mushroom powder (3 tbsp)
* 44 gr of nutritional yeast (about 5.5 tbsp)
* 22 gr of liquid smoke (about 2 tbsp)
* 50 gr of olive oil (about 4 tbsp)
* 300 gr of gluten (about 2.5 cups)

## Directions

1. Start boiling about 4 liters (or 4 quarts) of water in a wide pot.

1. Blend everything except the gluten, scraping the sides of the blender as
   needed to get a smooth mix.

   <img src="images/everything-in-blender.jpg" alt="Everything in the blender" width="200"/>

1. Put the mix in a large bowl. It should weight about 770 gr at this point.
   Add the gluten and mix until everything is combined and feels like a dough.

1. Take the dough to a surface and knead lightly until it feels completely
   smooth. It should be slightly sticky.

1. Roll the dough to shape it as a log and wrap it with aluminum foil. Twist
   the sides to make it as tight as possible.

1. Carefully put the dough roll in the pot with boiling water. Let it boil for
   1 hour.

1. Finally, carefully take it out of the pot and unwrap the aluminum foil using
   oven gloves. Let it cool down completely, preferably in the fridge
   overnight.

   <img src="images/rolled-and-boiled.jpg" alt="Hot seitan still wrapped after
   being taken out of the boiling water" width="300"/>
   <img src="images/cooling.jpg" alt="Cooling the seitan on top of a rack"
   width="300"/>

Enjoy!

<img src="images/sliced.jpg" alt="Seitan sliced very thin" width="300"/>
<img src="images/section.jpg" alt="Cut seitan log" width="300"/>
