---
title: "Mac & Cheesh"
summary: "A healthy adaptation of the American classic"
date: 2023-02-11T20:42:35-08:00
draft: false
time: "15m"
---

I originally started making mac & cheese from the [Kitchen Alchemy
recipe](https://blog.modernistpantry.com/recipes/rich-hearty-vegan-mac-cheese),
which is delicious but we felt like it had too much coconut oil and it was very
hard for digestion. Instead, I cut down on the oil and added a full block of
blended tofu instead for added nutritional value.

## Ingredients

* 100 gr water
* 350 gr unsweetened plant-based milk
* 10 gr tapioca starch
* 30 gr nutritional yeast
* 10 gr salt
* 2 gr garlic powder
* 2 gr onion powder
* 1 gr white pepper
* 3 gr lactic acid
* 75 gr melted coconut oil
* 1 block of (preferably silken) tofu
* 450 gr pasta (dried)

## Directions

1. Boil water and cook the pasta.

1. In a blender, mix all the other ingredients and blend on high until perfectly
smooth.

1. Move the mixture to a pan and place it over medium heat. Whisk until it
thickens.

1. Once the pasta is done, strain it and put it back in the pot it was cooked
in. Add the cheesh from the pan and mix well.

Enjoy!